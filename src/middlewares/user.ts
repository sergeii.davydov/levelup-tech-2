import { Request, Response, NextFunction } from "express";

import { IP2Location } from "ip2location-nodejs";

const ip2location = new IP2Location();

ip2location.open("./data/IP2LOCATION-LITE-DB3.IPV6.BIN");
ip2location.open("./data/IP2PROXY-LITE-PX3.BIN");

const parseIp = (req: Request) => {
  if (!Array.isArray(req.headers["x-forwarded-for"])) {
    return req.headers["x-forwarded-for"]?.split(",").shift();
  }

  return req.socket?.remoteAddress;
};

export const detectUserCountryByIP = (
  req: Request,
  _: Response,
  next: NextFunction
) => {
  const clientIp = parseIp(req);

  if (clientIp != null) {
    const country = ip2location.getCountryShort(clientIp);

    req.user = {
      ip: clientIp,
      country: country,
    };
  }

  next();
};
