declare namespace Express {
  interface Request {
    user?: {
      ip?: string;
      country?: string;
    };
  }
}
