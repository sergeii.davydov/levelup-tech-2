import { Router } from "express";
import { mapDomainByCountry, Domain } from "../helpers/config";

const router = Router();

router.get("/", (req, res) => {
  if (req.user?.country == null)
    return res.json({ mirror: Domain.MAIN, ...req.user });

  const currentDomain = mapDomainByCountry[req.user.country];

  if (currentDomain == null)
    return res.json({ mirror: Domain.MAIN, ...req.user });

  res.json({ mirror: currentDomain, ...req.user });
});

router.get("/ios", (req, res) => {
  if (req.user?.country == null)
    return res.json({ mirror: Domain.MAIN, ...req.user });

  const currentDomain = mapDomainByCountry[req.user.country];

  if (currentDomain == null)
    return res.json({ mirror: Domain.MAIN, ...req.user });

  res.json({ mirror: currentDomain, ...req.user });
});

export default router;
