import { Router, Request, Response } from "express";
import * as Sentry from "@sentry/node";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import crypto from "crypto";

import { Bonus } from "../models/Bonus";
import { getPrivateKey } from "../helpers/crypto";
import { SentryLevel } from "../helpers/sentry";

const router = Router();

const privateKey = getPrivateKey();

const Endpoint = {
  CashBonus: /^\/api\/bonus\/cash_bonuses(\/)?$/,
  CashBonusDeposits: /^\/api\/bonus\/cash_bonuses\/deposits(\/[0-9]+)?$/,
  FreeSpinsDeposits: /^\/api\/bonus\/freespins\/deposits(\/[0-9]+)?$/,
  LootBoxesDeposits: /^\/api\/bonus\/lootboxes\/deposits(\/[0-9]+)?$/,
  FreeSpins: /^\/api\/bonus\/freespins(\/)?$/,
  LootBoxes: /^\/api\/bonus\/lootboxes(\/)?$/,
};

const availableEndpoints = [
  Endpoint.CashBonus,
  Endpoint.CashBonusDeposits,
  Endpoint.FreeSpinsDeposits,
  Endpoint.LootBoxesDeposits,
  Endpoint.FreeSpins,
  Endpoint.LootBoxes,
];

const instance = axios.create({
  baseURL: "https://www.levelupcasino.com/",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

router.get("/bonuses", async (req: Request, res: Response) => {
  return Bonus.find()
    .then((bonuses) => res.json(bonuses))
    .catch((err) => res.status(500).send(JSON.stringify(err)));
});

router.post("/bonuses", async (req: Request, res: Response) => {
  try {
    const endpoint = req.headers.endpoint;

    if (
      endpoint == null ||
      Array.isArray(endpoint) ||
      !availableEndpoints.some((regExp) => regExp.test(endpoint))
    ) {
      Sentry.captureMessage("Endpoint not found", SentryLevel.Warning);
      return res.status(400).send("Endpoint not found");
    }

    Sentry.addBreadcrumb({
      category: "bonus api - endpoint",
      message: `Available endpoint ${endpoint}`,
      level: SentryLevel.Info,
    });

    const body = { ...req.body, external_id: uuidv4() };

    if (privateKey == null) {
      return res.status(500).send("Private key creation error");
    }

    const signature = crypto.sign(
      "RSA-SHA256",
      Buffer.from(JSON.stringify(body)),
      privateKey
    );

    Sentry.addBreadcrumb({
      category: "bonus api - signature",
      message: "Bonus api created signature",
      level: SentryLevel.Info,
    });

    await instance.post(endpoint, body, {
      headers: { Signature: signature.toString("base64") },
    });

    // await new Bonus({
    //   config: { endpoint, ...body },
    //   create_at: new Date(),
    // }).save();

    return res.status(201).send("The record was successfully created");
  } catch (error) {
    Sentry.captureException(error);
    return res.status(500).send(error);
  }
});

export default router;
