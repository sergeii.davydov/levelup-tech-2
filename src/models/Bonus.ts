import { model, Schema, Document } from "mongoose";

export interface IBonus extends Document {
  _id?: string;
  config: {
    user_ids?: string[];
    event?: string | undefined;
    external_id?: string;
    exp?: number;
    bonus?: {
      group?: string;
      title?: string;
      currencies?: [string];
      amount?: { [currencyCode: string]: number };
      wager?: number;
      duration?: number;
      activation_duration?: number;
      email_template?: string;
      max_win?: { [currencyCode: string]: number };
      games_category?: string;
      bonus_bet_limits?: { [currencyCode: string]: number };
      sticky?: boolean;
      growing_lock?: boolean;
    };
  };
  create_at: Date;
}

const BonusSchema = new Schema({
  user_ids: [String],
  config: {
    event: String,
    external_id: String,
    exp: Number,
    bonus: {
      group: String,
      title: String,
      currencies: [String],
      amount: Object,
      wager: Number,
      duration: Number,
      activation_duration: Number,
      email_template: String,
      max_win: Object,
      games_category: String,
      bonus_bet_limits: Object,
      sticky: Boolean,
      growing_lock: Boolean,
    },
  },
  create_at: { type: Date, required: true },
});

export const Bonus = model("bonuses", BonusSchema);
