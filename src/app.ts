import express from "express";
import cors from "cors";
import * as Sentry from "@sentry/node";

import { detectUserCountryByIP } from "./middlewares/user";

import { sentryInit, SentryLevel } from "./helpers/sentry";
// import { connectDatabase } from "./helpers/db";

import mirror from "./endpoints/app-mirror";
import customer from "./endpoints/customer";

sentryInit();

const app = express();

const port = process.env.PORT || 5001;
// const mongoUrl = process.env.MONGO_URL;

const main = async () => {
  try {
    // await connectDatabase(mongoUrl);

    app.use(cors());

    app.use(express.urlencoded());
    app.use(express.json());

    app.use(detectUserCountryByIP);

    app.use("/app-mirror", mirror);

    app.use("/customer", customer);

    app.get("/", (_, res) => {
      res.sendStatus(200);
    });

    app.listen(port, () => {
      Sentry.addBreadcrumb({
        category: "server",
        message: `The server is running on port ${port}`,
        level: SentryLevel.Info,
      });
      console.log(`The server is running on port ${port}`);
    });
  } catch (error) {
    Sentry.captureException(error);
    console.error(error);
  }
};

main();
