import * as Sentry from "@sentry/node";

const SENTRY_DSN = process.env.SENTRY_DSN;

export const sentryInit = () => {
  if (SENTRY_DSN == null) return;

  Sentry.init({
    dsn: SENTRY_DSN,
    tracesSampleRate: 1.0,
  });
};

export enum SentryLevel {
  Fatal = "fatal",
  Critical = "critical",
  Error = "error",
  Warning = "warning",
  Log = "log",
  Info = "info",
  Debug = "debug",
}
