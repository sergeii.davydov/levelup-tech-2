import mongoose from "mongoose";

export const connectDatabase = async (mongoUrl: string | undefined) => {
  try {
    if (mongoUrl == null) {
      throw Error(`Error connect to ${mongoUrl}`);
    }

    await mongoose.connect(mongoUrl);
    console.info(`Connected to database`);
  } catch (e) {
    throw Error(`Error connect to ${mongoUrl}`);
  }
};
