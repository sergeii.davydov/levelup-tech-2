import crypto from "crypto";
import * as Sentry from "@sentry/node";

const PRIVATE_KEY_BASE_64 = process.env.PRIVATE_KEY || "";

export const getPrivateKey = () => {
  try {
    const PRIVATE_KEY_BUFFER = new Buffer(PRIVATE_KEY_BASE_64, "base64");
    const PRIVATE_KEY = PRIVATE_KEY_BUFFER.toString("utf8");

    return crypto.createPrivateKey({ key: PRIVATE_KEY });
  } catch (error) {
    Sentry.captureException(error);
    return;
  }
};
